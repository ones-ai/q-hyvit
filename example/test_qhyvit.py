import sys

sys.path.insert(0, '..')
sys.path.insert(0, '.')
import torch
import torch.nn as nn
from tqdm import tqdm
import argparse
from importlib import reload, import_module
import multiprocessing
import os
import time
from itertools import product

import utils.datasets as datasets
import utils.net_wrap as net_wrap
from utils.quant_calib import QuantCalibrator, HessianQuantCalibrator
from utils.models import get_net
import pandas as pd

def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument('data', metavar='DIR',
                        help='path to dataset')
    parser.add_argument("--n_gpu", type=int, default=6)
    parser.add_argument("--multiprocess", action='store_true')
    args = parser.parse_args()
    return args


def test_classification(net, test_loader, max_iteration=None, description=None):
    pos = 0
    tot = 0
    i = 0
    max_iteration = len(test_loader) if max_iteration is None else max_iteration
    with torch.no_grad():
        q = tqdm(test_loader, desc=description)
        for inp, target in q:
            i += 1
            inp = inp.cuda()
            target = target.cuda()
            out = net(inp)
            pos_num = torch.sum(out.argmax(1) == target).item()
            pos += pos_num
            tot += inp.size(0)
            q.set_postfix({"acc": pos / tot})
            if i >= max_iteration:
                break
    print(pos / tot)
    return pos / tot


def process(pid, experiment_process, args_queue, n_gpu):
    """
    worker process.
    """
    gpu_id = pid % n_gpu
    os.environ['CUDA_VISIBLE_DEVICES'] = f'{gpu_id}'

    tot_run = 0
    while args_queue.qsize():
        test_args = args_queue.get()
        print(f"Run {test_args} on pid={pid} gpu_id={gpu_id}")
        experiment_process(**test_args)
        time.sleep(0.5)
        tot_run += 1
        # run_experiment(**args)
    print(f"{pid} tot_run {tot_run}")


def multiprocess(experiment_process, cfg_list=None, n_gpu=6):
    """
    run experiment processes on "n_gpu" cards via "n_gpu" worker process.
    "cfg_list" arranges kwargs for each test point, and worker process will fetch kwargs and carry out an experiment.
    """
    args_queue = multiprocessing.Queue()
    for cfg in cfg_list:
        args_queue.put(cfg)

    ps = []
    for pid in range(n_gpu):
        p = multiprocessing.Process(target=process, args=(pid, experiment_process, args_queue, n_gpu))
        p.start()
        ps.append(p)
    for p in ps:
        p.join()


def init_config(config_name):
    """initialize the config. Use reload to make sure it's fresh one!"""
    _, _, files = next(os.walk("../configs"))
    if config_name + ".py" in files:
        quant_cfg = import_module(f"configs.{config_name}")
    else:
        raise NotImplementedError(f"Invalid config name {config_name}")
    reload(quant_cfg)
    return quant_cfg


def experiment_basic(data='/ssd/dataset/imagenet', net='vit_base_patch16_384', config="PTQ4ViT"):
    """
    A basic testbench.
    """
    quant_cfg = init_config(config)
    name = net
    net = get_net(net)
    wrapped_modules = net_wrap.wrap_modules_in_net(net, quant_cfg)

    g = datasets.ViTImageNetLoaderGenerator(data, 'imagenet', 32, 32, 16, kwargs={"model": net})
    test_loader = g.test_loader()
    calib_loader = g.calib_loader(num=32)

    quant_calibrator = HessianQuantCalibrator(net, wrapped_modules, calib_loader, sequential=False,
                                              batch_size=4)  # 16 is too big for ViT-L-16

    quant_calibrator.quant_minmax_calib()
    print("minmax quantization done")

    # set before minmax quantization
    for _, module in quant_calibrator.wrapped_modules.items():
        module.mode = "raw"
        module.raw_grad = None


    # block-wise search setting
    if name in ['mobilevit_xxs', 'mobilevit_xs', 'mobilevit_s']:
        block_list = [17,18, 36,37, 67,68]
    elif name in ['mobilevitv2_050', 'mobilevitv2_075', 'mobilevitv2_100', 'mobilevitv2_125', 'mobilevitv2_150',
        'mobilevitv2_175', 'mobilevitv2_200']:
        block_list = [14,15, 28,29, 50,51]

    elif name in ['mobile_former_26m', 'mobile_former_52m', 'mobile_former_96m',
            'mobile_former_151m', 'mobile_former_214m', 'mobile_former_294m', 'mobile_former_508m']:
        dic = {'mobile_former_26m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129],
               'mobile_former_52m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129],
               'mobile_former_96m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129],
                'mobile_former_151m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129],
               'mobile_former_214m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129],
               'mobile_former_294m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129],
               'mobile_former_508m':[13,14, 19,20, 30,31, 46,47, 63,64, 69,70, 79,80, 85,86, 95,96, 101,102, 118,119, 128,129]}
        block_list = dic[name]

    elif name in ['efficientformer_l1', 'efficientformer_l3', 'efficientformer_l7']:
        dic = {'efficientformer_l1':[26,27,28], 'efficientformer_l3':[44,45,46], 'efficientformer_l7':[64,65,66]}
        block_list = dic[name]
    elif name in ['efficientformerv2_s0', 'efficientformerv2_s1', 'efficientformerv2_s2', 'efficientformerv2_l']:
        dic = {'efficientformerv2_s0':[15,16,17, 56,57, 58], 'efficientformerv2_s1':[21,22, 23, 71,72, 73], 'efficientformerv2_s2':[27,28, 29, 102,103, 104], 'efficientformerv2_l':[33,34,35, 133,134,135]}
        block_list = dic[name]
    else:
        block_list = []

    quant_calibrator.batching_hybrid_calib(block_list)

    # original
    print("hybrid quantization done")
    acc = test_classification(net, test_loader)
    return acc

if __name__ == '__main__':
    args = parse_args()
    cfg_list = []

    nets = [
        'mobilevit_xxs', 'mobilevit_xs', 'mobilevit_s',

        'mobilevitv2_050', 'mobilevitv2_075', 'mobilevitv2_100', 'mobilevitv2_125', 'mobilevitv2_150',
        'mobilevitv2_175', 'mobilevitv2_200',

        'efficientformer_l1', 'efficientformer_l3', 'efficientformer_l7',

        'efficientformerv2_s0', 'efficientformerv2_s1', 'efficientformerv2_s2', 'efficientformerv2_l',

        'mobile_former_26m', 'mobile_former_52m', 'mobile_former_96m', 'mobile_former_151m', 'mobile_former_214m',
        'mobile_former_294m', 'mobile_former_508m'
        ]

    configs = ['Block4Hybrid']
    elapsed_time_dict = {}

    cfg_list = [{
        "data": args.data,
        "net": net,
        "config": config,
    }
        for net, config in product(nets, configs)
    ]

    if args.multiprocess:
        multiprocess(experiment_basic, cfg_list, n_gpu=args.n_gpu)
    else:
        for cfg in cfg_list:
            start_time = time.time()

            acc = experiment_basic(**cfg)

            end_time = time.time()
            elapsed_time = end_time - start_time
            elapsed_time_dict[cfg["net"]] = {"elapsed_time": elapsed_time, "accuracy": acc, "config": cfg["config"]}

    # Convert Dict to DataFrame
    df = pd.DataFrame.from_dict(elapsed_time_dict, orient='index')

    # DataFrame to CSV file
    csv_file_path = "./elapsed_time_dict.csv"
    df.to_csv(csv_file_path)

    # Load CSV to DF
    loaded_df = pd.read_csv(csv_file_path, index_col=0)

    # Print DF
    print(loaded_df)