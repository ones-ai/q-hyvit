# Q-HyViT

Recently, vision transformers (ViTs) have superseded convolutional neural networks in numerous applications, including classification, detection, and segmentation. However, the high computational requirements of ViTs hinder their widespread implementation. To address this issue, researchers have proposed efficient hybrid transformer architectures that combine convolutional and transformer layers with optimized attention computation of linear complexity. Additionally, post-training quantization has been proposed as a means of mitigating computational demands. For mobile devices, achieving optimal acceleration for ViTs necessitates the strategic integration of quantization techniques and efficient hybrid transformer structures. However, no prior investigation has applied quantization to efficient hybrid transformers.
In this work, we discover that applying existing post-training quantization (PTQ) methods for ViTs to efficient hybrid transformers leads to a drastic accuracy drop, attributed to the four following challenges:  (i) highly dynamic ranges, (ii) zero-point overflow, (iii) diverse normalization, and (iv) limited model parameters ($<$5M). 
To overcome these challenges, we propose a new post-training quantization method, which is the first to quantize efficient hybrid ViTs (MobileViTv1, MobileViTv2, Mobile-Former, EfficientFormerV1, EfficientFormerV2). We achieve a significant improvement of 17.73% for 8-bit and 29.75% for 6-bit on average, respectively, compared with existing PTQ methods (EasyQuant, FQ-ViT, PTQ4ViT, and RepQ-ViT)}.

## Run

### Requirements:  
torch==1.13.1  
timm=0.9.2   

### Data:
Download the ILSVRC12 ImageNet classification dataset.

### Pretrained models:
For most models, pretrained versions can be automatically downloaded via the timm library.  

To facilitate the use of Mobile-Former, we have pre-downloaded the weights from the [MobileFormer GitHub Repository](https://github.com/AAboys/MobileFormer) and included them in this repository. Therefore, please note that the git clone process might be somewhat slow.

### Steps to run:
Update the data path in the script to point to your local dataset location. To run the script, use the following command:
```bash
python example/test_qhyvit.py [path to imagenet]
```

## Results

| Model                | # Params. | Type       | FP32 | EasyQuant W8A8 | EasyQuant W6A6 | PTQ4ViT W8A8 | PTQ4ViT W6A6 | RepQ-ViT W8A8 | RepQ-ViT W6A6 | Ours W8A8 | Ours W6A6 |
|----------------------|-----------|------------|------|----------------|----------------|--------------|--------------|---------------|---------------|-----------|-----------|
| MobileViTv1-xss      | 1.3M      | Hybrid     | 69.0 | 36.13          | 10.17          | 37.75        | 30.80        | 1.85          | 1.38          | 68.20     | 66.33     |
| MobileViTv1-xs       | 2.3M      | Hybrid     | 74.8 | 73.16          | 55.22          | 65.52        | 62.56        | 41.96         | 27.29         | 74.31     | 73.44     |
| MobileViTv1-s        | 5.6M      | Hybrid     | 78.4 | 74.21          | 42.70          | 68.19        | 65.07        | 59.01         | 56.61         | 77.92     | 77.18     |
| MobileViTv2-050      | 1.4M      | Hybrid     | 70.2 | 66.90          | 11.58          | 39.39        | 45.38        | 26.60         | 27.89         | 69.89     | 69.07     |
| MobileViTv2-075      | 2.8M      | Hybrid     | 75.6 | 62.81          | 2.54           | 65.54        | 65.85        | 55.52         | 40.61         | 75.82     | 74.58     |
| MobileViTv2-100      | 4.9M      | Hybrid     | 78.1 | 69.34          | 0.12           | 51.02        | 47.27        | 40.85         | 26.07         | 77.63     | 77.11     |
| MobileViTv2-125      | 7.5M      | Hybrid     | 79.6 | 77.31          | 4.56           | 67.39        | 59.39        | 41.65         | 30.43         | 79.31     | 77.03     |
| MobileViTv2-150      | 10.6M     | Hybrid     | 80.4 | 75.93          | 10.39          | 68.61        | 67.58        | 62.12         | 58.71         | 80.09     | 79.97     |
| MobileViTv2-175      | 14.3M     | Hybrid     | 80.8 | 79.83          | 47.22          | 72.30        | 71.78        | 63.52         | 62.89         | 80.63     | 80.45     |
| MobileViTv2-200      | 18.5M     | Hybrid     | 81.2 | 80.04          | 57.32          | 75.50        | 74.65        | 64.65         | 62.15         | 80.94     | 80.76     |
| Mobile-Former-26m    | 3.2M      | Hybrid     | 64.0 | 28.95          | 0.12           | 58.27        | 47.25        | 0.11          | 0.16          | 61.78     | 51.06     |
| Mobile-Former-52m    | 3.5M      | Hybrid     | 68.7 | 62.16          | 17.29          | 67.32        | 62.01        | 1.12          | 1.00          | 67.79     | 62.65     |
| Mobile-Former-96m    | 4.6M      | Hybrid     | 72.8 | 53.31          | 33.68          | 71.32        | 64.72        | 0.40          | 0.25          | 71.60     | 64.21     |
| Mobile-Former-151m   | 7.6M      | Hybrid     | 75.2 | 4.98           | 3.49           | 73.86        | 68.16        | 0.11          | 0.12          | 74.30     | 68.44     |
| Mobile-Former-214m   | 9.4M      | Hybrid     | 76.7 | 72.19          | 28.32          | 75.01        | 68.24        | 1.05          | 0.14          | 75.76     | 69.34     |
| Mobile-Former-294m   | 11.4M     | Hybrid     | 77.9 | 74.75          | 59.55          | 76.96        | 74.48        | 0.13          | 0.58          | 76.93     | 74.6      |
| Mobile-Former-506m   | 14.0M     | Hybrid     | 79.3 | 78.01          | 67.14          | 75.44        | 70.13        | 0.19          | 0.26          | 75.60     | 74.67     |
| EfficientFormerV1-L1 | 12.3M     | MetaBlock  | 80.2 | 78.24          | 58.83          | 80.11        | 79.8         | 80.36         | 78.55         | 80.15     | 77.25     |
| EfficientFormerV1-L3 | 31.3M     | MetaBlock  | 82.4 | 82.39          | 80.38          | 82.39        | 82.36        | 82.41         | 82.29         | 82.46     | 82.18     |
| EfficientFormerV1-L7 | 82.1M     | MetaBlock  | 83.3 | 83.24          | 81.89          | 83.34        | 83.28        | 83.28         | 83.03         | 83.31     | 83.12     |
| EfficientFormerV2-S0 | 3.5M      | Hybrid     | 76.2 | 68.21          | 41.24          | 68.40        | 41.26        | 40.02         | 37.11         | 74.69     | 74.18     |
| EfficientFormerV2-S1 | 6.1M      | Hybrid     | 79.7 | 66.42          | 2.69           | 73.44        | 73.34        | 58.30         | 53.06         | 77.56     | 77.54     |
| EfficientFormerV2-S2 | 12.6M     | Hybrid     | 82.0 | 71.80          | 7.02           | 79.85        | 79.39        | 70.39         | 70.37         | 80.62     | 80.30     |
| EfficientFormerV2-L  | 26.1M     | Hybrid     | 83.5 | 80.34          | 3.34           | 82.46        | 82.22        | 76.72         | 74.33         | 82.80     | 82.71     |


| Model                           | # Params. | Type   | FP32 | FQ-ViT | Ours  |
|---------------------------------|-----------|--------|------|--------|-------|
| MobileViTv1-xxs (MVv1-xxs)      | 1.3M      | Hybrid | 68.91| 0.1    | 67.20 |
| MobileViTv1-xs (MVv1-xs)        | 2.3M      | Hybrid | 74.64| 62.2   | 73.89 |
| MobileViTv1-s (MVv1-s)          | 5.6M      | Hybrid | 78.31| 74.94  | 77.72 |
| MobileViTv2-050 (MVv2-050)      | 1.4M      | Hybrid | 70.16| 5.00   | 68.73 |
| MobileViTv2-075 (MVv2-075)      | 2.8M      | Hybrid | 75.62| 34.60  | 74.36 |
| MobileViTv2-100 (MVv2-100)      | 4.3M      | Hybrid | 78.09| 0.40   | 77.13 |



## Acknowledgement
The original code is borrowed from [PTQ4VIT](https://github.com/hahnyuan/ptq4vit) and [FQ-ViT](https://github.com/megvii-research/FQ-ViT).  


## Citation
Please cite the following paper if you find the implementation beneficial for your work. We would appreciate it.

```bibtex
@article{lee2023q,
  title={Q-HyViT: Post-Training Quantization for Hybrid Vision Transformer with Bridge Block Reconstruction for IoT Systems},
  author={Lee, Jemin and Kwon, Yongin and Park, Jeman and Yu, Misun and Park, Sihyeong and Song, Hwanjun},
  journal={https://doi.org/10.48550/arXiv.2303.12557},
  year={2024}
}
```